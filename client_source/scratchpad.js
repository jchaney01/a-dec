var sugar = require('sugar'),
    assert = require('assert');

var data = {
    "name": "Highspeed Air",
    "displayName": "Highspeed Air Handpieces",
    "description": "Get the precision, performance, comfort and control you want and need with A-dec|W&H highspeed air handpieces. Plus, you'll experience reduced hand and wrist fatigue with our best swivel and the lightest system in the industry.",
    "bullets": "&bull; gross enamel reduction &bull; finishing &bull; polishing<br>&bull; crown and bridge &bull; amalgam removal",
    "tooltips": {
        "headOptions": "<div class='ttCloseBtn'></div><div class='title'>How to choose the head type</div><p>The right head type for you will align with your goals and needs. The smaller the head, the better the access and visibility. The larger the head, the higher the cutting power. Consider these differences:</p><p><strong>Micro:</strong> Best access and visibility with limited cutting power \r\n<strong>Pedo:</strong> Most popular, with improved access and visibility \r\n<strong>Standard:</strong> Extra cutting power <strong>Torque:</strong> Maximum cutting power</p>",
        "light": "TDB",
        "sprayPorts": "TDB"
    },
    "series": [{
        "name": "SYNEA 400",
        "rating": "Best",
        "image": "http://placehold.it/300x200",
        "light": {"description": "Ring LED+", "image": "http://placehold.it/300x180"},
        "sprayPorts": {"description": "5", "image": "http://placehold.it/300x180"},
        "tubing": {"description": "6-pin", "image": "http://placehold.it/300x180"},
        "warranty": {"description": "2-year"},
        "heads": [{
            "name": "Micro",
            "models": [{
                "name": "TK-97L",
                "rating": "Best",
                "mostPopular": true,
                "brand": "A-dec|W&H",
                "coupler": "A-dec|W&H",
                "watts": 18,
                "light": "Ring LED+",
                "sprayPorts": 5,
                "tubing": "6-pin",
                "warranty": "2 years",
                "recommended": true,
                "partNumber": "0.30016001",
                "msrp": 1675,
                "chuck": "1.6 mm friction grip burs",
                "relatedProducts": [{
                    "name": "RQ-24",
                    "rating": "Best",
                    "mostPopular": true,
                    "brand": "A-dec|W&H",
                    "coupler": "requirements",
                    "watts": 18,
                    "light": "Ring LED+",
                    "sprayPorts": 5,
                    "tubing": "6-pin",
                    "warranty": "2 years",
                    "recommended": true,
                    "partNumber": "0.30016001",
                    "msrp": 1675,
                    "chuck": "1.6 mm friction grip burs"
                }],
                "$$hashKey": "00L",
                "active": true
            }, {
                "name": "AD-56M",
                "rating": "Best",
                "mostPopular": true,
                "brand": "A-dec|W&H",
                "coupler": "Another one",
                "watts": 18,
                "light": "Ring LED+",
                "sprayPorts": 5,
                "tubing": "6-pin",
                "warranty": "2 years",
                "recommended": true,
                "partNumber": "0.30016001",
                "msrp": 1675,
                "chuck": "1.6 mm friction grip burs",
                "relatedProducts": [{
                    "name": "RQ-24",
                    "rating": "Best",
                    "mostPopular": true,
                    "brand": "A-dec|W&H",
                    "coupler": "requirements",
                    "watts": 18,
                    "light": "Ring LED+",
                    "sprayPorts": 5,
                    "tubing": "6-pin",
                    "warranty": "2 years",
                    "recommended": true,
                    "partNumber": "0.30016001",
                    "msrp": 1675,
                    "chuck": "1.6 mm friction grip burs"
                }],
                "$$hashKey": "00M",
                "active": true
            }],
            "$$hashKey": "009",
            "active": true
        }, {
            "name": "Pedo",
            "models": [{
                "name": "AK-92L",
                "rating": "Best",
                "mostPopular": true,
                "brand": "A-dec|W&H",
                "coupler": "requirements",
                "watts": 18,
                "light": "Ring LED+",
                "sprayPorts": 5,
                "tubing": "6-pin",
                "warranty": "2 years",
                "recommended": true,
                "partNumber": "0.30016001",
                "msrp": 1675,
                "chuck": "1.6 mm friction grip burs",
                "relatedProducts": [{
                    "name": "RQ-24",
                    "rating": "Best",
                    "mostPopular": true,
                    "brand": "A-dec|W&H",
                    "coupler": "requirements",
                    "watts": 18,
                    "light": "Ring LED+",
                    "sprayPorts": 5,
                    "tubing": "6-pin",
                    "warranty": "2 years",
                    "recommended": true,
                    "partNumber": "0.30016001",
                    "msrp": 1675,
                    "chuck": "1.6 mm friction grip burs"
                }],
                "active": true,
                "$$hashKey": "00F"
            }],
            "$$hashKey": "00A",
            "active": true
        }],
        "$$hashKey": "007"
    }]
};

var log = console.log;

/**
 * Look recursively into an object for a specific property on the end of the chain of property names
 * provided. This could be more robust.
 *
 * @example var result = recursiveGetMatchingProp(data, ['series', 'heads', 'models'], 'active', true);
 *
 * @param {object} obj A Javascript object
 * @param {array} chain List of names to recurse through to look for the final property, i.e. ['series', 'heads', 'models']
 * @param {string} prop Name of the property to check, i.e. 'active'
 * @param {bool|number|string} testValue Value to compare against for prop
 * @return {Array}
 */
function recursiveGetMatchingProp(obj, chain, prop, testValue) {

    var matches = [],
        paths = [];

    function recurse(obj, keys, index, dict) {
        var index = index || 0,
            dict = dict || {};
        if (Object.isObject(obj)) {
            if (keys.length) {
                var key = keys.shift();
                dict[key] = index;
                if (Object.isArray(obj[key])) {
                    var current = obj[key];
                    current.each(function (level, index) {
                        recurse(level, keys.clone(), index, dict);
                    });
                }
            } else {
                //log('item ' + obj.name, prop, 'has prop', prop in obj, 'prop val', obj[prop], 'indexes', dict, 'index', index);
                if (prop in obj && obj[prop] === testValue) {
                    matches.push(obj);
                    var path = Object.clone(dict);
                    path.index = index;
                    paths.push(path);
                }
            }
        }
    }

    recurse(obj, chain.clone());
    log(paths);
    return matches;
}

var result = recursiveGetMatchingProp(data, ['series', 'heads', 'models'], 'active', true);

//log(result);
//log(JSON.stringify(data));
