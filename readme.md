# A-dec Handpiece Selector Client-side Application
## By [Jason Chaney](mailto:jchaney@creativefew.com) for CMD

# Technologies
* AngularJS
* Sass
* Bower
* Node
* Grunt

# Architecture
The app follows some common documented conventions.  <https://github.com/johnpapa/angularjs-styleguide> outlines the common ideas and reasons for why code is architected the way it is.  This app also uses the <http://yeoman.io> workflow.

## Router
The router has routes set up for each category.  It ensures that API data is loaded before loading any state.

## API Service
The API Service is responsible for providing the category services with bootstrap data as well as tracking the active category.

## Category Service
There are services for each category.  They bootstrap themselves and then become the authoritative source of data for the app.  Their primary responsibility is to provide the category controllers with data, exposing getters/setters and communicating step integrity.  Step Integrity is a method on each Category Service to determine if that service has enough data to display a given step. 

## Category Controllers
These controllers manage the view model for each category.

## Form
The form collects the chosen (active) products from the specified (via URL) service as well as user data.  It uses the API Service to send this data to the backend for persistence.

## Summary
This view will display a summary of chosen products that are loaded via the API Service that the backend stored from the Form.

## Styles
Styles are grouped per category and each file contains styles namespaces to each step.  This results in some extra markup for the flexibility of customizing each step of each category.  There is also a "widgets" file containing self-contained components that are used throughout the app.  "main.scss" collects all Sass files which is compiled by Grunt.

## Build and Local Development
See Yeoman's documentation for build steps and local development.  You will run these commands to run the app locally:

    npm install
    bower install
    grunt serve
    
#Deployment
Deployment is done by running 

    grunt build
    
and uploading the contents of the resulting "dist" directory to a web server. 

##Staging
Host: 216.151.6.27
User: jchaney
Pass: *#yGW7d1

##Production
Host: 216.151.1.195
User: jchaney
Pass: *#yGW7d1