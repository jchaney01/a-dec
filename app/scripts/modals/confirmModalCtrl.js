'use strict';
/** @ngInject */
function ConfirmModalController($log, $scope, $modalInstance, itemName, onConfirm, onCancel) {
    $log.debug('ConfirmModalController: Init');
    $scope.itemName = itemName;
    $scope.onConfirm = onConfirm;
    $scope.onCancel = onCancel;
}

