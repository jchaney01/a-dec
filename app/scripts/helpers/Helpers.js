'use strict';
/** @ngInject */
function Helpers($log) {
  $log.debug('Helpers: Init');
  return  {
    getObjectByPath:getObjectByPath,
    recursiveGetMatchingProp:recursiveGetMatchingProp
  };

  /**
   * Retrieve objects or values by dot notation paths
   *
   * @example var result = getObjectByPath(data, 'customers.locations.machines');
   *
   * @param {object} obj A Javascript object
   * @param {Array} path List of names to recurse through to look for the final property/object,
   * i.e. 'customers.locations.machines'
   * @param {Array} props Name of the properties to retrieve as an array of strings, optionally named with `as`,
   *                i.e. `"deep.nested.property as property"`
   * @param {object} [params]
   * @param {bool} [params.clone] Use clone when duplicating (untested)
   * @oaram {bool} [params.keepSource] Attach original unfiltered object before filtering as __source
   * @oaram {bool} [params.sourceAs] Set the name of the original unfilitered source object
   * @oaram {bool} [params.keepParents] Attach a parent property to the child objects (only for use with `props[]`)
   * @return {Array|String|bool|Number}
   */

  function getObjectByPath(obj, path, props, params) {

    function recurse(obj, keys, memo, parent) {
      Object.isArray(memo) || (memo = []);
      if (Object.isString(keys)) {
        keys = keys.split('.');
      }
      if (Object.isObject(obj)) {
        params.keepParents && parent && (obj.parent = parent);
        if (keys.length) {
          var key = keys.shift();
          if (key in obj) {
            var current = obj[key];
            if (Object.isArray(current)) {
              current.each(function (innerObj) {
                return recurse(innerObj, Object.clone(keys), memo, obj);
              });
            } else {
              return recurse(current, Object.clone(keys), memo);
            }
          }
        } else {
          memo.push(obj);
        }
      } else {
        memo.push(obj);
      }
      return flatten(memo);
    }

    var matches = recurse(obj, path);

    if (Object.isArray(props)) {
      var matcher = /([a-zA-Z\.]+)(( as )([a-zA-Z\.]+))?/;
      matches = matches.map(function (match) {
        var item = {};
        params.keepSource && (item[params['sourceAs'] || '__source'] = match);
        props.each(function (prop) {
          var pieces = prop.match(matcher);
          var matched = pieces && 5 === pieces.length && ' as ' === pieces[3];
          var isPath = pieces[0].indexOf('.') !== -1;
          var matchPropVal = (matched || isPath) ? recurse(match, pieces[1]) : match[prop];
          var key = (matched ? pieces[4] : prop);
          item[key] = params.clone ? Object.clone(matchPropVal) : matchPropVal;
        });
        return item;
      });
    }
    if (Object.isFunction(params.filter)) {
      matches = matches.filter(params.filter);
    }

    if (Object.isFunction(params.transform)) {
      matches = matches.map(params.transform);
    }

    return flatten(matches);

    function flatten(obj) {
      if (!params.flatten) {
        return obj;
      }
      if (!Object.isArray(obj)) {
        return obj;
      }
      return obj.length > 1 || 0 === obj.length ? obj : obj[0];
    }
  }
  /**
   *
   * Utility function to look recursively into an object for a specific property on the end of the chain of property names
   * provided. This could be more robust.
   *
   * @example var result = recursiveGetMatchingProp(data, ['series', 'heads', 'models'], 'active', true);
   *
   * @param {object} obj A Javascript object
   * @param {Array} chain List of names to recurse through to look for the final property, i.e. ['series', 'heads', 'models']
   * @param {string} prop Name of the property to check, i.e. 'active'
   * @param {bool|number|string} testValue Value to compare against for prop
   * @return {Array}
   */
  function recursiveGetMatchingProp(obj, chain, prop, testValue) {
    var matches = [];

    function recurse(obj, keys, index, dict) {
      var index = index || 0,
          dict = dict || {};
      if (Object.isObject(obj)) {
        if (keys.length) {
          var key = keys.shift();
          var pos = chain.length - 1 - keys.length;
          dict[key] = index;
          if (Object.isArray(obj[key])) {
            var current = obj[key];
            current.each(function (level, index) {
              recurse(level, keys.clone(), index, dict);
            });
          }
        } else {
          if (prop in obj && obj[prop] === testValue) {
            matches.push(obj);
          }
        }
      }
    }

    recurse(obj, chain.clone());
    return matches;
  }
}
