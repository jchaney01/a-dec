'use strict';

/** @ngInject */
function StickyStepBtnDirective ( $log, $modal,$timeout ) {
    return {
        restrict: 'A',
        templateUrl: 'scripts/stickyButtons/stepStickyBtnTmpl.html',
        replace: true,
        scope: {
            "desc": "@",
            "step":"@step",
            "disabled":'=disabledo',
            "onClicked":'&'
        },
        compile: function ( tElement, tAttrs, transclude ) {
            return function ( $scope, $element, $attributes ) {
            }
        }
    }
}
