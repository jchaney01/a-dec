'use strict';
angular.module('sticky', [])

/** @ngInject */
.directive('sticky', function($timeout,$log,$window,$interval){
	return {
		restrict: 'A',
		scope: {
			offset: '@'
		},
		link: function($scope, $elem, $attrs){
            function checkSticky(){
                var scrollPosition = window.pageYOffset;
                var windowSize     = window.innerHeight;
                var bodyHeight     = document.body.offsetHeight;
                var headerHeight = $('header').innerHeight();
                var scrollBottom = (Math.max(bodyHeight - (scrollPosition + windowSize), 0));
                var footerHeight = $('footer').innerHeight();
                var contentHeight = $('.main').innerHeight();
                var totalContentHeight = contentHeight+headerHeight-30;
                var ctaSectionHeight = $('.rightCol').innerHeight();

                if (windowSize < headerHeight+ctaSectionHeight+$elem.innerHeight() && scrollPosition < headerHeight){
                    $log.debug("true");
                    return $elem.css('position','static');
                }
                $elem.css('position','fixed');
                if ( scrollBottom <= footerHeight && $("body").height() > $(window).height()){ //scrollbar - near bottom
                    //$log.debug("we have a scrollbar but are near bottom");
                    $elem.css('bottom',(footerHeight-scrollBottom)+25+'px');
                } else if ($("body").height() < $(window).height()){ //no scrollbar
                    //            $log.debug("we have no scrollbar");
                    //            $log.debug("headerHeight:"+headerHeight);
                    //            $log.debug("footerHeight:"+footerHeight);
                    //            $log.debug("contentHeight:"+contentHeight);
                    //            $log.debug("totalContentHeight:"+totalContentHeight);
                    //            $log.debug("windowHeight:"+$(window).height());
                    $elem.css('bottom',$(window).height()-totalContentHeight-10+'px');

                } else {
                    //$log.debug("we have a scrollbar and are not near the bottom");
                    $elem.css('bottom',$scope.offset+'px');
                }
            }
			$timeout(function(){

                $elem.css('position', 'fixed');
                        $elem.css('bottom', $scope.offset+'px');

                $timeout(function(){
                  $elem.animate({opacity:1});
                },100);
                angular.element($window).on('scroll resize',checkSticky);
                checkSticky();
            },100);
            $interval(checkSticky,1000); //Android likes to break stuff on pinch zoom, this just makes sure it's where it needs to be regardless
		}
	};
});
