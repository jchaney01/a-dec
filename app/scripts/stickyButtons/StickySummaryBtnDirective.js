'use strict';

/** @ngInject */
function StickySummaryDirective ( $log, $modal,$timeout ) {
    return {
        restrict: 'A',
        templateUrl: 'scripts/stickyButtons/stickySummaryTmpl.html',
        replace: true,
        scope: {},
        compile: function ( tElement, tAttrs, transclude ) {
            return function ( $scope, $element, $attributes ) {
            }
        }
    }
}
