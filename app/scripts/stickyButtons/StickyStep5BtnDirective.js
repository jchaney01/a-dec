'use strict';

/** @ngInject */
function StickyStep5BtnsDirective ( $log, $modal,$timeout ) {
    return {
        restrict: 'A',
        templateUrl: 'scripts/stickyButtons/stickyStep5BtnTmpl.html',
        replace: true,
        scope: {
            "onSaveShareClicked":'&'
        },
        compile: function ( tElement, tAttrs, transclude ) {
            return function ( $scope, $element, $attributes ) {
            }
        }
    }
}
