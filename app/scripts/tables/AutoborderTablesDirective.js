'use strict';

//This directive will add vertical dividers to an element based on a child elements table.
//Assumes this directive is applied to an entry with thumbnail

/** @ngInject */
function AutoBorderDirective ( $log,$timeout ) {
    return {
        restrict: 'A',
        compile: function ( tElement, tAttrs, transclude ) {
            return function ( $scope, $element, $attributes ) {
              $log.debug("AutoBorder: Init");
              //cache the second table
              var table = $($element).find('table')[1];
              var entryWidth = $element.width();
              //grab all TDs of the second table
              var allTds = $(table).find('td');
              //Grab the size of the thumbnail
              $timeout(function(){
                var thumbnailSize = $element.find('.thumb').height();
                $log.debug(thumbnailSize);
              },50);
              //Loop over each
                //create a divider
                //append it to the DOM
                //calculate the correct position by adding up all td's before it
                //move it to the correct spot

            }
        }
    }
}
