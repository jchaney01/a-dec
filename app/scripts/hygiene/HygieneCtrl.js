'use strict';

/** @ngInject */
function HygieneController($log,HygieneService,$state,$modal,$timeout,constants,$stateParams) {
    $log.debug('HygieneController: Init');
    /* jshint validthis: true */
    var vm = this;

    activate();

    function activate(){
      vm.title = HygieneService.getTitle();
      vm.intro = HygieneService.getIntro();
      vm.tooltips = HygieneService.getTooltips();
      vm.series = HygieneService.getSeries();
      vm.extras = HygieneService.getExtras();
      vm.stepIntegrity = HygieneService.stepIntegrity;
      vm.clickNextStep = clickNextStep;
      vm.chosenProducts = HygieneService.getActiveModels();
      vm.allProducts = HygieneService.getActiveProducts();
      vm.toggleProduct = toggleProductActiveState;
      vm.clickShare = clickShare;
    }

    function clickNextStep(step){
      $log.debug('HygieneController: clickNextStep called');
      if (vm.stepIntegrity(step)){
        $state.go('hygiene',{
          'step':step
        });
      } else {
        var modalInstance = $modal.open ( {
          templateUrl: 'scripts/modals/stepRejection.html',
          windowClass:'confirmModal'
        });
          modalInstance.opened.then(function(){
              $timeout(function(){
                  adjustModalMaxHeightAndPosition();
              },constants().timeouts.modals);
          });
      }
    }

    function checkIfNeedToRedirectToStep2(){
      vm.chosenProducts = HygieneService.getActiveModels();
      vm.allProducts = HygieneService.getActiveProducts();
      if (vm.allProducts.length == 0){
        var modalInstance = $modal.open ( {
          templateUrl: 'scripts/modals/notifyRedirectToStep2.html',
          controller: function($scope,onConfirm){
            $scope.onConfirm = onConfirm;
          },
          size: 'sm',
          windowClass:'centerModal',
          resolve: {
            onConfirm: function () {
              return function() {
                $state.go('hygiene',{
                  'step':2
                });
              }
            }
          }
        });
      }
    }

    function toggleProductActiveState(model,type){
        $log.debug("HygieneController: Toggling active state for "+type,model);
      function getModalTemplatePath(){
        if ($stateParams.step == 5 && model.active && model.type == 'model' && HygieneService.doesProductHaveAnyActiveRelatedProducts(model)){
          return 'scripts/modals/removeModelRelatedNotice.html';
        } else {
          return 'scripts/modals/modelRemoval.html';
        }
      }
      var modalInstance = $modal.open ( {
        templateUrl: getModalTemplatePath(),
        controller: function($scope,onConfirm, onCancel){
          $scope.onConfirm = onConfirm;
          $scope.onCancel = onCancel;
        },
        size: 'sm',
        windowClass:'centerModal',
        resolve: {
          onConfirm: function () {
            return function() {
              deactivate(model,type);
              checkIfNeedToRedirectToStep2();
            }
          },
          onCancel: function () {
            return function() {

            }
          }
        }
      });

    }

    function deactivate(model,type){
      HygieneService.deactivate(model,type);
      vm.chosenProducts = HygieneService.getActiveModels();
      vm.allProducts = HygieneService.getActiveProducts();
    }

    function clickShare(){
      $state.go('summary-form',{
        'category':'hygiene'
      });
    }
}

