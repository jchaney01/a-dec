'use strict';
var oldIE;
(function ($) {
    "use strict";

    // Detecting IE
    if ($('html').is('.ie6, .ie7, .ie8')) {
        oldIE = true;
    }

    if (oldIE) {
        // Here's your JS for IE..
    } else {
        // ..And here's the full-fat code for everyone else
    }

}(jQuery));

angular
    .module('adec', [
      'ui.router',
      'ngSanitize',
      'ui.bootstrap',
      'sticky',
      'angular-google-analytics'
    ]);

angular
    .module('adec')
    /** @ngInject */
    .controller('AppController' , AppController)
    /** @ngInject */
    .factory('ApiService' , ApiService)
    /** @ngInject */
    .factory('HomeService' , HomeService)
    /** @ngInject */
    .factory('Helpers' , Helpers)
    /** @ngInject */
    .factory('ImagePreloadFactory' , ImagePreloadFactory)
    /** @ngInject */
    .factory('HighSpeedAirService' , HighSpeedAirService)
    /** @ngInject */
    .factory('MaintenanceService' , MaintenanceService)
    /** @ngInject */
    .factory('LowSpeedAirService' , LowSpeedAirService)
    /** @ngInject */
    .factory('HighSpeedElectricService' , HighSpeedElectricService)
    /** @ngInject */
    .factory('LowSpeedElectricService' , LowSpeedElectricService)
    /** @ngInject */
    .factory('HygieneService' , HygieneService)
    /** @ngInject */
    .factory('ImagePreloadFactory' , ImagePreloadFactory)
    /** @ngInject */
    .directive('navigation' , NavigationDirective)
    /** @ngInject */
    .directive('checkBox' , CheckboxDirective)
    /** @ngInject */
    .directive('adTooltip' , TooltipDirective)
    /** @ngInject */
    .directive('thumbnailDirective' , ThumbnailDirective)
    /** @ngInject */
    .directive('stickyStepBtn' , StickyStepBtnDirective)
    /** @ngInject */
    .directive('stickyStep5Btns' , StickyStep5BtnsDirective)
    /** @ngInject */
    .directive('stickySummaryBtns' , StickySummaryDirective)
    /** @ngInject */
    .directive('autoBorder' , AutoBorderDirective)
    /** @ngInject */
    .constant('constants',function(){
        //Defaults
        var constants = {
            "timeouts":{
                "modals":115
            }
        };
        //IE8 is having issues with the centering modal delay
        if (oldIE){
            constants.timeouts.modals = 100;
        }
        return constants;
    })
    /** @ngInject */
    .filter('noFractionCurrency',
    [ '$filter', '$locale',
      function(filter, locale) {
        var currencyFilter = filter('currency');
        var formats = locale.NUMBER_FORMATS;
        return function(amount, currencySymbol) {
          var value = currencyFilter(amount, currencySymbol);
          var sep = value.indexOf(formats.DECIMAL_SEP);
          if(amount >= 0) {
            return value.substring(0, sep);
          }
          return value.substring(0, sep) + ')';
        };
      } ])

    /** @ngInject */
    .config(Router)
    /** @ngInject */
    .run(function($rootScope, $state, $stateParams,Analytics){
      $rootScope.$state = $state;
      $rootScope.$stateParams = $stateParams;
      FastClick.attach(document.body);
    });




function adjustModalMaxHeightAndPosition(){
  $('.modal').each(function(){
    if($(this).hasClass('in') == false){
      $(this).show();
    };
    var contentHeight = $(window).height() - 60;
    var headerHeight = $(this).find('.modal-header').outerHeight() || 2;
    var footerHeight = $(this).find('.modal-footer').outerHeight() || 2;

    $(this).find('.modal-content').css({
      'max-height': function () {
        return contentHeight;
      }
    });

    $(this).find('.modal-body').css({
      'max-height': function () {
        return (contentHeight - (headerHeight + footerHeight));
      }
    });

    $(this).find('.modal-dialog').css({
      'margin-top': function () {
        return -($(this).outerHeight() / 2);
      },
      'margin-left': function () {
        return -($(this).outerWidth() / 2);
      }
    });
    if($(this).hasClass('in') == false){
      $(this).hide();
    };
  });
};

if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
  var msViewportStyle = document.createElement("style");
  msViewportStyle.appendChild(
      document.createTextNode(
          "@-ms-viewport{width:auto!important}"
      )
  );
  document.getElementsByTagName("head")[0].appendChild(msViewportStyle)
}





