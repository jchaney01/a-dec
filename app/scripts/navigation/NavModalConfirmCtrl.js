'use strict';
/** @ngInject */
function NavModalConfirmController($log, $scope, $modalInstance, text,onConfirm,onClose) {
  $log.debug('NavModalConfirmController: Init');
  $scope.text = text;
  $scope.onClose = onClose;
  $scope.onConfirm = onConfirm;
}