'use strict';
/** @ngInject */
function NavigationDirective($log,$http,$compile,$stateParams,$state,$modal,ApiService,HighSpeedAirService,LowSpeedAirService,HighSpeedElectricService,LowSpeedElectricService,HygieneService,MaintenanceService,$timeout) {
  return {
    restrict: 'AC',
    scope: true,
    compile: function (tElement, tAttrs, transclude) {
      return function ($scope, $element, $attributes) {
        $http.get('scripts/navigation/navigation.html',{cache:true}).then(function(data) {
          var compiled = $compile(angular.element(data.data));
          var linked = compiled($scope);
          $element.append(linked);
        });
        $scope.getClassForStep = function(step){
          if ($stateParams.step > step){
            return 'complete';
          }
          if ($stateParams.step < step){
            return 'incomplete';
          }
          if ($stateParams.step == step || ($state.current.name == 'home' && step == 1)){
            return 'current';
          }
        };

        function stepIntegrety(step){
          switch($state.current.name){
            case 'hsAir':
              return HighSpeedAirService.stepIntegrity(step);
              break;
            case 'lsAir':
              return LowSpeedAirService.stepIntegrity(step);
              break;
            case 'hsElectric':
              return HighSpeedElectricService.stepIntegrity(step);
              break;
            case 'lsElectric':
              return LowSpeedElectricService.stepIntegrity(step);
              break;
            case 'hygiene':
              return HygieneService.stepIntegrity(step);
              break;
            case 'maintenance':
              return MaintenanceService.stepIntegrity(step);
              break;
            default:
              return false;
          }
        }

        //checks
        function tryingToAdvanceTooFarFromHome(targetStep){
          return $state.current.name=="home" && targetStep > 2 && !stepIntegrety(targetStep);
        }
        function tryingToAdvanceTooFarForward(targetStep){
          return $stateParams.step > targetStep + 2 && $state.current.name != "home" && targetStep > $stateParams.step;
        }
        function tryingToAdvanceWithoutRequiredData(targetStep){
          return !stepIntegrety(targetStep) && $state.current.name != "home";
        }
        function tryingToAdvanceToStep2FromHomeWithoutChoosingCategory(){
          return !ApiService.getChosenCategory() && $state.current.name == "home";
        }
        function tryingToAdvanceForward(targetStep){
          return targetStep >= $stateParams.step || $state.current.name == "home";
        }


        $scope.navItemClicked = function(targetStep){
          if (tryingToAdvanceForward(targetStep)){
            return;
          }
          if (tryingToAdvanceTooFarFromHome(targetStep)){
            $log.debug("NavigationDirective: tryingToAdvanceTooFarFromHome");
            var modalInstance = $modal.open ( {
              controller: NavModalController,
              templateUrl: 'scripts/navigation/navModal.html',
              size: 'sm',
              windowClass:'confirmModal',
              resolve: {
                text: function () {
                  return 'You need to choose features first before you can go to step '+targetStep+".";
                },
                onClose: function() {
                  return function() {

                  }
                }
              }
            });
            modalInstance.opened.then(function(){
              $timeout(function(){
                adjustModalMaxHeightAndPosition();
              },15);
            });
            return;
          }
          if (tryingToAdvanceTooFarForward(targetStep)){
            $log.debug("NavigationDirective: tryingToAdvanceTooFarForward");
            var modalInstance = $modal.open ( {
              controller: NavModalController,
              templateUrl: 'scripts/navigation/navModal.html',
              size: 'sm',
              windowClass:'confirmModal',
              resolve: {
                text: function () {
                  return "You can't skip to step "+targetStep+" without making some choices first.";
                },
                onClose: function() {
                  return function() {

                  }
                }
              }
            });
            modalInstance.opened.then(function(){
              $timeout(function(){
                adjustModalMaxHeightAndPosition();
              },15);
            });
            return;
          }
          if (tryingToAdvanceWithoutRequiredData(targetStep)){
            $log.debug("NavigationDirective: tryingToAdvanceWithoutRequiredData");
            var modalInstance = $modal.open ( {
              controller: NavModalConfirmController,
              templateUrl: 'scripts/navigation/navModalConfirm.html',
              size: 'sm',
              windowClass:'confirmModal',
              resolve: {
                text: function () {
                  if ($stateParams.step == 2){
                    return "You can't move to step "+targetStep+" without making some choices first.";
                  } else {
                    return "You can't move to step "+targetStep+" without making some choices first. Would you like to go to Step 2?";
                  }
                },
                onClose: function() {
                  return function() {

                  }
                },
                onConfirm: function() {
                  return function() {
                    $state.go(ApiService.getChosenCategory(), {step: 2})
                  }
                }
              }
            });
            modalInstance.opened.then(function(){
              $timeout(function(){
                adjustModalMaxHeightAndPosition();
              },15);
            });
            return;
          }
          if (tryingToAdvanceToStep2FromHomeWithoutChoosingCategory(targetStep)){
            $log.debug("NavigationDirective: tryingToAdvanceToStep2FromHomeWithoutChoosingCategory");
            var modalInstance = $modal.open ( {
              controller: NavModalController,
              templateUrl: 'scripts/navigation/navModal.html',
              size: 'sm',
              windowClass:'confirmModal',
              resolve: {
                text: function () {
                  return 'You need to choose a category before you can choose features.';
                },
                onClose: function() {
                  return function() {

                  }
                }
              }
            });
            modalInstance.opened.then(function(){
              $timeout(function(){
                adjustModalMaxHeightAndPosition();
              },15);
            });
            return;
          }

          $state.go(ApiService.getChosenCategory(), {step: targetStep})
        }
      }
    }
  }
}
