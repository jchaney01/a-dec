'use strict';

/** @ngInject */
function MaintenanceController($log,MaintenanceService,$state,$modal,$timeout,constants,$stateParams) {
    $log.debug('MaintenanceController: Init');
    /* jshint validthis: true */
    var vm = this;

    activate();

    function activate(){
      vm.models = MaintenanceService.getModels();
      vm.title = MaintenanceService.getTitle();
      vm.intro = MaintenanceService.getIntro();
      vm.tooltips = MaintenanceService.getTooltips();
      vm.chosenProducts = MaintenanceService.getActiveModels();
      vm.allProducts = MaintenanceService.getActiveProducts();
      vm.stepIntegrity = MaintenanceService.stepIntegrity;
      vm.clickNextStep = clickNextStep;
      vm.toggleProduct = toggleProductActiveState;
      vm.clickShare = clickShare;
    }

  function clickNextStep(step){
    $log.debug('MaintenanceController: clickNextStep called');
    if (vm.stepIntegrity(step)){
      $state.go('maintenance',{
        'step':step
      });
    } else {
      var modalInstance = $modal.open ( {
        templateUrl: 'scripts/modals/stepRejection.html',
        windowClass:'confirmModal'
      });
        modalInstance.opened.then(function(){
            $timeout(function(){
                adjustModalMaxHeightAndPosition();
            },constants().timeouts.modals);
        });
    }
  }

  function clickShare(){
    $state.go('summary-form',{
      'category':'maintenance'
    });
  }

  function checkIfNeedToRedirectToStep2(){
    vm.chosenProducts = MaintenanceService.getActiveModels();
    vm.allProducts = MaintenanceService.getActiveProducts();
    if (vm.chosenProducts.length == 0){
      var modalInstance = $modal.open ( {
        templateUrl: 'scripts/modals/notifyRedirectToStep2.html',
        controller: function($scope,onConfirm){
          $scope.onConfirm = onConfirm;
        },
        size: 'sm',
        windowClass:'centerModal',
        resolve: {
          onConfirm: function () {
            return function() {
              $state.go('maintenance',{
                'step':2
              });
            }
          }
        }
      });
    }
  }

  function toggleProductActiveState(model,type){
    $log.debug("MaintenanceController: Toggling active state for "+type,model);
    //If on step 5 they remove a model, alert them that all related products will be removed as well
    //If after the removal there are no products, alert them why they are being redirected.
    function getModalTemplatePath(){
      if ($stateParams.step == 5 && model.active && model.type == 'model' && MaintenanceService.doesProductHaveAnyActiveRelatedProducts(model)){
        return 'scripts/modals/removeModelRelatedNotice.html';
      } else {
        return 'scripts/modals/modelRemoval.html';
      }
    }
    var modalInstance = $modal.open ( {
      templateUrl: getModalTemplatePath(),
      controller: function($scope,onConfirm, onCancel){
        $scope.onConfirm = onConfirm;
        $scope.onCancel = onCancel;
      },
      size: 'sm',
      windowClass:'centerModal',
      resolve: {
        onConfirm: function () {
          return function() {
            deactivate(model,type);
            checkIfNeedToRedirectToStep2();
          }
        },
        onCancel: function () {
          return function() {

          }
        }
      }
    });

  }

  function deactivate(model,type){
    MaintenanceService.deactivate(model,type);
    vm.chosenProducts = MaintenanceService.getActiveModels();
    vm.allProducts = MaintenanceService.getActiveProducts();
  }

}

