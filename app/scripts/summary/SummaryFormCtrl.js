'use strict';

/** @ngInject */
function SummaryFormController($log,$stateParams,HighSpeedAirService,$timeout,$state,ApiService,LowSpeedAirService,HighSpeedElectricService,LowSpeedElectricService,MaintenanceService,HygieneService) {
    $log.debug('SummaryFormController: Init');
    /* jshint validthis: true */
    var vm = this;
    activate();

    function activate(){
      switch ($stateParams.category){
        case 'hsAir':
          vm.activeProducts = HighSpeedAirService.getActiveProducts();
          break;
        case 'lsAir':
          vm.activeProducts = LowSpeedAirService.getActiveProducts();
          break;
        case 'hsElectric':
          vm.activeProducts = HighSpeedElectricService.getActiveProducts();
          break;
        case 'lsElectric':
          vm.activeProducts = LowSpeedElectricService.getActiveProducts();
          break;
        case 'maintenance':
          vm.activeProducts = MaintenanceService.getActiveProducts();
          break;
        case 'hygiene':
          vm.activeProducts = HygieneService.getActiveProducts();
          break;
        default:
          $log.error("SummaryFormController: No state param preset to derive active products");
      }
      vm.sendData = sendData;
      $log.debug("SummaryFormController: Data captured to send",vm.activeProducts);
    }

    function sendData(){
      vm.loading = true;
      $log.debug("SummaryFormController: Active products before server response",vm.activeProducts);
      ApiService.sendData(prepareData(vm.userData)).then(function(data){
        vm.loading = false;
        $log.debug("SummaryFormController: Received data from API service",data);
        var id = data.data.unique_id;
        $log.debug("SummaryFormController: Active products after server response",vm.activeProducts);
        $state.go('summary',{
          id:id
        },{
          reload: true,
          inherit: false,
          notify: true
        });
      },function(data){
        vm.loading = false;
        $log.debug("SummaryFormController: Received error from API service",data);
        vm.validationError = data.data.message;
        vm.errorField = data.data.field;
        if (data.data.field == 'chosenProducts'){
          alert('No products are available to save. Please report this error: Error summary form ctrl:sendData()')
        }
      });
    }

    function prepareData(data){
      $log.debug("SummaryFormController: Preparing data",data);
      if (!data){
        data = {};
      }
      if (vm.activeProducts){
        $log.debug("SummaryFormController: Adding activeProducts to payload",vm.activeProducts);
        data.chosenProducts = angular.copy(vm.activeProducts);
      }
      data.chosenProducts.each(function(product){
        delete product.parent;
        delete product.related_products;
      });
      return data;
    }

}

