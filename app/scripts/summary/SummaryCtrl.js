'use strict';

/** @ngInject */
function SummaryController($log,$stateParams,products) {
    $log.debug('SummaryController: Init');
    /* jshint validthis: true */
    var vm = this;
    activate();

    function activate(){
      vm.products = products.data.data;
      $log.debug("SummaryController: Received products from server",vm.products);
      if ($stateParams.confirm){
        setTextForConfirm();
      } else {
        setTextForSummary();
      }
    }

    function setTextForConfirm(){
      vm.headline = "Here's a confirmation of your products"
    }

    function setTextForSummary(){
      vm.headline = "Your summary has been sent!"
    }
}

