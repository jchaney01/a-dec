'use strict';

/** @ngInject */
function HsAirController($log,HighSpeedAirService,$state,$modal,$timeout,$scope,$stateParams,constants) {
    $log.debug('HsAirController: Init');
    /* jshint validthis: true */
    var vm = this;

    activate();

    function activate(){
      vm.title = HighSpeedAirService.getTitle();
      vm.intro = HighSpeedAirService.getIntro();
      vm.tooltips = HighSpeedAirService.getTooltips();
      vm.series = getSeries();
      vm.extras = HighSpeedAirService.getExtras();
      vm.stepIntegrity = HighSpeedAirService.stepIntegrity;
      vm.clickNextStep = clickNextStep;
      vm.chosenProducts = HighSpeedAirService.getActiveModels();
      vm.allProducts = HighSpeedAirService.getActiveProducts();
      vm.toggleProduct = toggleProductActiveState;
      vm.clickShare = clickShare;
    }

    function getSeries(){
      return HighSpeedAirService.getSeries()
    }

    function clickNextStep(step){
      $log.debug('HsAirController: clickNextStep called');
      if (vm.stepIntegrity(step)){
        $state.go('hsAir',{
          'step':step
        });
      } else {
        var modalInstance = $modal.open ( {
          templateUrl: 'scripts/modals/stepRejection.html',
          windowClass:'confirmModal'
        });
        modalInstance.opened.then(function(){
          $timeout(function(){
            adjustModalMaxHeightAndPosition();
          },constants().timeouts.modals);
        });
      }
    }

    function checkIfNeedToRedirectToStep2(){
      vm.chosenProducts = HighSpeedAirService.getActiveModels();
      vm.allProducts = HighSpeedAirService.getActiveProducts();
      if (vm.allProducts.length == 0){
        var modalInstance = $modal.open ( {
          templateUrl: 'scripts/modals/notifyRedirectToStep2.html',
          controller: function($scope,onConfirm){
            $scope.onConfirm = onConfirm;
          },
          size: 'sm',
          windowClass:'centerModal',
          resolve: {
            onConfirm: function () {
              return function() {
                $state.go('hsAir',{
                  'step':2
                });
              }
            }
          }
        });
      }
    }

    function toggleProductActiveState(model,type){
        $log.debug("HsAirController: Toggling active state for "+type,model);

        function getModalTemplatePath(){
          if ($stateParams.step == 5 && model.active && model.type == 'model' && HighSpeedAirService.doesProductHaveAnyActiveRelatedProducts(model)){
            return 'scripts/modals/removeModelRelatedNotice.html';
          } else {
            return 'scripts/modals/modelRemoval.html';
          }
        }

        var modalInstance = $modal.open ( {
          templateUrl: getModalTemplatePath(),
          controller: function($scope,onConfirm, onCancel){
            $scope.onConfirm = onConfirm;
            $scope.onCancel = onCancel;
          },
          size: 'sm',
          windowClass:'centerModal',
          resolve: {
            onConfirm: function () {
              return function() {
                deactivate(model,type);
                checkIfNeedToRedirectToStep2();
              }
            },
            onCancel: function () {
              return function() {

              }
            }
          }
        });
    }

    function deactivate(model,type){
      HighSpeedAirService.deactivate(model,type);
      vm.chosenProducts = HighSpeedAirService.getActiveModels();
      vm.allProducts = HighSpeedAirService.getActiveProducts();
    }

    function clickShare(){
      $state.go('summary-form',{
        'category':'hsAir'
      });
    }
}

