'use strict';

/** @ngInject */
function LsAirController($log,LowSpeedAirService,$state,$modal,$timeout,constants,$stateParams) {
    $log.debug('LsAirController: Init');
    /* jshint validthis: true */
    var vm = this;

    activate();

    function activate(){
      vm.title = LowSpeedAirService.getTitle();
      vm.intro = LowSpeedAirService.getIntro();
      vm.tooltips = LowSpeedAirService.getTooltips();
      vm.motorSystems = getMotorSystems();
      vm.extras = LowSpeedAirService.getExtras();
      vm.stepIntegrity = LowSpeedAirService.stepIntegrity;
      vm.clickNextStep = clickNextStep;
      vm.chosenProducts = LowSpeedAirService.getActiveModels();
      vm.toggleProduct = toggleProductActiveState;
      vm.allProducts = LowSpeedAirService.getActiveProducts();
      vm.clickShare = clickShare;
    }

    function getMotorSystems(){
      return LowSpeedAirService.getMotorSystems();
    }

    function clickNextStep(step){
      $log.debug('LsAirController: clickNextStep called');
      if (vm.stepIntegrity(step)){
        $state.go('lsAir',{
          'step':step
        });
      } else {
        var modalInstance = $modal.open ( {
          templateUrl: 'scripts/modals/stepRejection.html',
          windowClass:'confirmModal'
        });
          modalInstance.opened.then(function(){
              $timeout(function(){
                  adjustModalMaxHeightAndPosition();
              },constants().timeouts.modals);
          });
      }
    }

    function toggleProductActiveState(model,type){
      $log.debug("LsAirController: Toggling active state for "+type,model);
      function getModalTemplatePath(){
        if ($stateParams.step == 5 && model.active && model.type == 'model' && LowSpeedAirService.doesProductHaveAnyActiveRelatedProducts(model)){
          return 'scripts/modals/removeModelRelatedNotice.html';
        } else {
          return 'scripts/modals/modelRemoval.html';
        }
      }

      var modalInstance = $modal.open ( {
        templateUrl: getModalTemplatePath(),
        controller: function($scope,onConfirm, onCancel){
          $scope.onConfirm = onConfirm;
          $scope.onCancel = onCancel;
        },
        size: 'sm',
        windowClass:'centerModal',
        resolve: {
          onConfirm: function () {
            return function() {
              deactivate(model,type);
              checkIfNeedToRedirectToStep2();
            }
          },
          onCancel: function () {
            return function() {

            }
          }
        }
      });

    }

    function deactivate(model,type){
      LowSpeedAirService.deactivate(model,type);
      vm.chosenProducts = LowSpeedAirService.getActiveModels();
      vm.allProducts = LowSpeedAirService.getActiveProducts();
    }

    function checkIfNeedToRedirectToStep2(){
      vm.chosenProducts = LowSpeedAirService.getActiveModels();
      vm.allProducts = LowSpeedAirService.getActiveProducts();
      if (vm.allProducts.length == 0){
        var modalInstance = $modal.open ( {
          templateUrl: 'scripts/modals/notifyRedirectToStep2.html',
          controller: function($scope,onConfirm){
            $scope.onConfirm = onConfirm;
          },
          size: 'sm',
          windowClass:'centerModal',
          resolve: {
            onConfirm: function () {
              return function() {
                $state.go('lsAir',{
                  'step':2
                });
              }
            }
          }
        });
      }
    }

    function clickShare(){
      $state.go('summary-form',{
        'category':'lsAir'
      });
    }

}

