'use strict';

/** @ngInject */
function HsElectricController($log,HighSpeedElectricService,$state,$modal,$timeout,constants,$stateParams) {
    $log.debug('HsElectricController: Init');
    /* jshint validthis: true */
    var vm = this;

    activate();

    function activate(){
      vm.title = HighSpeedElectricService.getTitle();
      vm.intro = HighSpeedElectricService.getIntro();
      vm.tooltips = HighSpeedElectricService.getTooltips();
      vm.series = HighSpeedElectricService.getSeries();
      vm.stepIntegrity = HighSpeedElectricService.stepIntegrity;
      vm.extras = HighSpeedElectricService.getExtras();
      vm.clickNextStep = clickNextStep;
      vm.chosenProducts = HighSpeedElectricService.getActiveModels();
      vm.allProducts = HighSpeedElectricService.getActiveProducts();
      vm.toggleProduct = toggleProductActiveState;
      vm.clickShare = clickShare;
    }

    function clickNextStep(step){
      $log.debug('HsElectricController: clickNextStep called');
      if (vm.stepIntegrity(step)){
        $state.go('hsElectric',{
          'step':step
        });
      } else {
        var modalInstance = $modal.open ( {
          templateUrl: 'scripts/modals/stepRejection.html',
          windowClass:'confirmModal'
        });
      modalInstance.opened.then(function(){
          $timeout(function(){
              adjustModalMaxHeightAndPosition();
          },constants().timeouts.modals);
      });
      }
    }

    function toggleProductActiveState(model,type){
      $log.debug("HsElectricController: Toggling active state for "+type,model);
      function getModalTemplatePath(){
        if ($stateParams.step == 5 && model.active && model.type == 'model' && HighSpeedElectricService.doesProductHaveAnyActiveRelatedProducts(model)){
          return 'scripts/modals/removeModelRelatedNotice.html';
        } else {
          return 'scripts/modals/modelRemoval.html';
        }
      }

      var modalInstance = $modal.open ( {
        templateUrl: getModalTemplatePath(),
        controller: function($scope,onConfirm, onCancel){
          $scope.onConfirm = onConfirm;
          $scope.onCancel = onCancel;
        },
        size: 'sm',
        windowClass:'centerModal',
        resolve: {
          onConfirm: function () {
            return function() {
              deactivate(model,type);
              checkIfNeedToRedirectToStep2();
            }
          },
          onCancel: function () {
            return function() {

            }
          }
        }
      });
    }

    function deactivate(model,type){
      HighSpeedElectricService.deactivate(model,type);
      vm.chosenProducts = HighSpeedElectricService.getActiveModels();
      vm.allProducts = HighSpeedElectricService.getActiveProducts();
    }

    function checkIfNeedToRedirectToStep2(){
      vm.chosenProducts = HighSpeedElectricService.getActiveModels();
      vm.allProducts = HighSpeedElectricService.getActiveProducts();
      if (vm.allProducts.length == 0){
        var modalInstance = $modal.open ( {
          templateUrl: 'scripts/modals/notifyRedirectToStep2.html',
          controller: function($scope,onConfirm){
            $scope.onConfirm = onConfirm;
          },
          size: 'sm',
          windowClass:'centerModal',
          resolve: {
            onConfirm: function () {
              return function() {
                $state.go('hsElectric',{
                  'step':2
                });
              }
            }
          }
        });
      }
    }

  function clickShare(){
    $state.go('summary-form',{
      'category':'hsElectric'
    });
  }

}

