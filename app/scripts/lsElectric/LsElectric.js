'use strict';

/** @ngInject */
function LsElectricController($log,LowSpeedElectricService,$state,$modal,$timeout,constants,$stateParams) {
    $log.debug('LsElectricController: Init');
    /* jshint validthis: true */
    var vm = this;

    activate();

    function activate(){
      vm.title = LowSpeedElectricService.getTitle();
      vm.intro = LowSpeedElectricService.getIntro();
      vm.tooltips = LowSpeedElectricService.getTooltips();
      vm.series = LowSpeedElectricService.getSeries();
      vm.stepIntegrity = LowSpeedElectricService.stepIntegrity;
      vm.clickNextStep = clickNextStep;
      vm.extras = LowSpeedElectricService.getExtras();
      vm.chosenProducts = LowSpeedElectricService.getActiveModels();
      vm.toggleProduct = toggleProductActiveState;
      vm.allProducts = LowSpeedElectricService.getActiveProducts();
      vm.clickShare = clickShare;
    }

    function clickNextStep(step){
      $log.debug('LsElectricController: clickNextStep called');
      if (vm.stepIntegrity(step)){
        $state.go('lsElectric',{
          'step':step
        });
      } else {
        var modalInstance = $modal.open ( {
          templateUrl: 'scripts/modals/stepRejection.html',
          windowClass:'confirmModal'
        });
          modalInstance.opened.then(function(){
              $timeout(function(){
                  adjustModalMaxHeightAndPosition();
              },constants().timeouts.modals);
          });
      }
    }

    function toggleProductActiveState(model,type){
      $log.debug("LsAirController: Toggling active state for "+type,model);

      function getModalTemplatePath(){
        if ($stateParams.step == 5 && model.active && model.type == 'model' && LowSpeedElectricService.doesProductHaveAnyActiveRelatedProducts(model)){
          return 'scripts/modals/removeModelRelatedNotice.html';
        } else {
          return 'scripts/modals/modelRemoval.html';
        }
      }

      var modalInstance = $modal.open ( {
        templateUrl: getModalTemplatePath(),
        controller: function($scope,onConfirm, onCancel){
          $scope.onConfirm = onConfirm;
          $scope.onCancel = onCancel;
        },
        size: 'sm',
        windowClass:'centerModal',
        resolve: {
          onConfirm: function () {
            return function() {
              deactivate(model,type);
              checkIfNeedToRedirectToStep2();
            }
          },
          onCancel: function () {
            return function() {

            }
          }
        }
      });

    }

    function deactivate(model,type){
      LowSpeedElectricService.deactivate(model,type);
      vm.chosenProducts = LowSpeedElectricService.getActiveModels();
      vm.allProducts = LowSpeedElectricService.getActiveProducts();
    }

    function checkIfNeedToRedirectToStep2(){
      vm.chosenProducts = LowSpeedElectricService.getActiveModels();
      vm.allProducts = LowSpeedElectricService.getActiveProducts();
      if (vm.allProducts.length == 0){
        var modalInstance = $modal.open ( {
          templateUrl: 'scripts/modals/notifyRedirectToStep2.html',
          controller: function($scope,onConfirm){
            $scope.onConfirm = onConfirm;
          },
          size: 'sm',
          windowClass:'centerModal',
          resolve: {
            onConfirm: function () {
              return function() {
                $state.go('lsElectric',{
                  'step':2
                });
              }
            }
          }
        });
      }
    }

  function clickShare(){
    $state.go('summary-form',{
      'category':'lsElectric'
    });
  }

}

