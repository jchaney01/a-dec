'use strict';

//This directive takes a "product" object.  A product is an item that a user can toggle on and off and can optionally
//contain associated "models".

/** @ngInject */
function CheckboxDirective ( $log, $modal,$timeout,constants ) {
    return {
        restrict: 'A',
        templateUrl: 'scripts/checkers/checkboxTmpl.html',
        replace: true,
        scope: {
            "product": "=",
            "name":"@", //OPTIONAL - Will use this instead of the name bound to the product
            "appendPrice":"@", //OPTIONAL - Will add a second line and is the price of the product
            "checkOnly":"@" //OPTIONAL - Will only display the checkbox itself
        },
        compile: function ( tElement, tAttrs, transclude ) {
            return function ( $scope, $element, $attributes ) {
                $scope.shouldBluifyName = shouldBluifyName;
                $scope.oneModelsExists=oneModelsExists;
                $scope.clickedTitle=clickedTitle;
              
                $element.find('.first').on ( 'click', function () {

                  //If this product is inactive:
                  if(!$scope.product.active){
                    //Activate product
                    $scope.product.active = true;
                    //if one model, activate first model (we have only one model)
                    if (oneModelsExists()){
                      $scope.product.models[0].active = true;
                    }
                    //if multiple models, open modal (we need to activate one or more models and at least two exist)
                    if (multipleModelsExist()){
                      openModalForProduct();
                    }
                  } else {
                    //if multiple models, confirm otherwise just wipe
                    if (multipleModelsExist()){
                      confirmDeactivation();
                    } else {
                      deactivateModels();
                    }
                  }
                  $scope.$apply();

                } );

                function multipleModelsExist () {
                    return $scope.product.hasOwnProperty('models') && $scope.product.models.length > 1;
                }

                function oneModelsExists () {
                    return $scope.product.hasOwnProperty('models') && $scope.product.models.length == 1;
                }

                function anyModelsExists () {
                    return $scope.product.hasOwnProperty('models') && $scope.product.models.length >= 1;
                }

                function openModalForProduct () {
                    $log.debug("CheckboxDirective: Opening modal for product.");
                    var modalInstance = $modal.open ( {
                        templateUrl: 'scripts/checkers/modalGeneralTpl.html', //May need to be specific to current category
                        controller: GeneralModalController, //May need to be specific as well
                        size: 'md',
                        keyboard:false,
                        backdrop:'static',
                        windowClass:'centerModal',
                        resolve: {
                            models: function () {
                                return $scope.product.models;
                            },
                            onClose: function () {
                              return function() {
                                if (!getActiveModels()){
                                  deactivateModels();
                                }
                              }
                            }
                        }
                    });
                    modalInstance.opened.then(function(){
                        $timeout(function(){
                            adjustModalMaxHeightAndPosition();
                        },constants().timeouts.modals);
                    });

                }

                function confirmDeactivation ( ) {
                    var modalInstance = $modal.open ( {
                        controller: ConfirmModalController,
                        templateUrl: 'scripts/modals/confirmModelDeactivationModal.html',//---> Build this out with a normal "confirmation modal"
                        size: 'sm',
                        windowClass:'confirmModal',
                        resolve: {
                            itemName: function () {
                                return $scope.product.name;
                            },
                            onConfirm: function() {
                                return function() {
                                    deactivateModels();
                                }
                            },
                            onCancel: function () {
                                return function() {
                                    $log.debug ( 'user did not confirm deactivation' );
                                }
                            }
                        }
                    });
                }

                function deactivateModels() {
                    if (anyModelsExists()) {
                        $scope.product.models.each ( function ( model ) {
                            model.active = false;
                        } );
                    }
                    $scope.product.active = false;
                }

                function getActiveModels(){
                  if (anyModelsExists()) {
                    if ($scope.product.models.find({active:true}) == undefined){
                      return false;
                    } else {
                      return $scope.product.models.find({active:true});
                    }
                  }
                }

                function shouldBluifyName() {
                  return $scope.product.active && getActiveModels() && multipleModelsExist();
                }

                function clickedTitle(){
                  if ($scope.product.active && getActiveModels() && multipleModelsExist()){
                    openModalForProduct();
                  }
                }

            }
        }
    }
}
