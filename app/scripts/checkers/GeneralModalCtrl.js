'use strict';

//GeneralModalController.$inject = ['$log','$scope','$modalInstance','models','onClose'];

/** @ngInject */
function GeneralModalController($log, $scope, $modalInstance, models,onClose) {
  $log.debug('GeneralModalController: Init');
  $scope.models = models;
  $scope.onClose = onClose;
}