'use strict';
/** @ngInject */
function HighSpeedAirService($log,$timeout,$http,$q,$state,$rootScope,ApiService,Helpers,ImagePreloadFactory) {
  $log.debug('HighSpeedAirService: Init');
  var data;
  return  {
    debug:debug,
    getData:getData,
    getTitle:getDisplayName,
    getExtras:getExtras,
    getIntro:getDescription,
    getSeries:getSeries,
    getTooltips:getTooltips,
    getActiveModels:getActiveModels,
    getActiveProducts:getActiveProducts,
    stepIntegrity:stepIntegrity,
    deactivate:deactivate,
    preloadImages:preloadImages,
    doesProductHaveAnyActiveRelatedProducts:doesProductHaveAnyActiveRelatedProducts
  };

  function debug(){
    return data;
  }

  function getData(){
    return ApiService.getData().then(function(res){
      return data = res.categories[0];
    });
  }

  function getDisplayName(){
    return data.type + ' ' + data.name + ' Handpieces';
  }

  function getDescription(){
    return data.description;
  }

  function getSeries(){
    return data.series;
  }

  function getTooltips(){
        return data.tooltips;
    }

  //Returns a flattened array of all products (models, related products and extras)
  function getActiveProducts(){
    //make output array
    var output = [];
    //Get active models
    var models = getActiveModels();
    //add each model to the output array as well as all active related products for each model
    models.each(function(model){
      model.type = "model";
      output.push(model);
      if (model.related_products){
        var relatedActive = model.related_products.findAll({
          "active":true
        });
        relatedActive.each(function(relatedProduct){
          relatedProduct.type = 'related';
          output.push(relatedProduct);
        });
      }
    });
    var extras = getActiveExtras();
    extras.each(function(model){
      model.type = "extra";
      output.push(model);
    });
    return output;
  }

  //Determines if a thing (model or related product) has any active related products
  function doesProductHaveAnyActiveRelatedProducts(product){
    if (product.related_products){
      if (product.related_products.find({'active':true})){
        return true;
      }
    }
    return false;
  }

  //Returns all models that are either active or have an active related product
  function getActiveModels() {
      //grabs all models, filters by active OR
      return Helpers.getObjectByPath(data,
          'series.heads.models',
          "*",
          {
              clone: false,
              flatten: false,
              // easiest way to get to higher items, series.name in this case would be model.parent.name
              keepParents: true,
              // make sure either the product itself is active
              // or that it has at least one active related product to be included
              filter: function filterActive(product) {
                  return product.active;
              }
          }
      );
  }

    /**
   *
   * Determine if there enough information is present to advance to a specific step.
   *
   * @example if(stepIntegrity(3));
   *
   * @param {number} step The step number to check
   * @return {bool}
   */
  function stepIntegrity(step){
    if (step <= 2 && data==undefined){
      return false;
    }
    if (step >= 3){
      if (getActiveProducts().length == 0){
        return false;
      }
    }
    return true;
  }

  function shouldDeactivateHead(head){
    if (head != undefined){
      return !head.models.findAll({'active':true}).length >= 1
    }
  }

  function deactivate(item,type){
    if (type == "model"){
      deactivateModel(item)
    }
    if (type == "related"){
      deactivateProduct(item)
    }
    if (type == "extra"){
      deactivateExtra(item)
    }
  }

  function deactivateModel(model){
    data.series.each(function(series){
      if (series.heads){
        series.heads.each(function(head){
          if (head.models.find(model)){
            head.models.find(model).active = false;
            if (shouldDeactivateHead(head)){
              head.active = false;
            }
            deactivateAllRelatedProductsForModel(model);
          }
        });
      }
    });
    return true;
  }

  function deactivateAllRelatedProductsForModel(model){
    if (model.related_products){
      model.related_products.each(function(product){
        product.active = false;
      })
    }
  }

  function deactivateProduct(product){
     product.active = false;
     return true;
  }

  function deactivateExtra(product){
     product.active = false;
     return true;
  }

  function preloadImages(){
    getData().then(function(){
      var loader = ImagePreloadFactory.createInstance();
      var products = Helpers.getObjectByPath(data,'series.heads.models','thumbnail_image_url',{
        clone:false,
        flatten:false,
        keepParents:false
      });
      loader.addImages(products.map('thumbnail_image_url'));
      loader.addImages(products.map('light_image_url'));
      loader.addImages(products.map('spray_ports_image_url'));
      loader.addImages(products.map('tubing_image_url'));
      loader.start(function(){
        $log.debug("HighSpeedAirService: Preloading complete");
      });
    });
  }

  //EXTRAS
  function getExtras(){
    return data.extras;
  }

  function getActiveExtras(){
    return data.extras.findAll({active:true});
  }

}
