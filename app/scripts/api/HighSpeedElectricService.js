'use strict';
/** @ngInject */
function HighSpeedElectricService($log,$timeout,$http,ImagePreloadFactory,$q,$state,$rootScope,ApiService,Helpers) {
  $log.debug('HighSpeedElectricService: Init');
  var data;
  return  {
    getData:getData,
    getTitle:getDisplayName,
    getIntro:getDescription,
    getSeries:getSeries,
    getTooltips:getTooltips,
    stepIntegrity:stepIntegrity,
    getActiveModels:getActiveModels,
    getExtras:getExtras,
    getActiveProducts:getActiveProducts,
    deactivate:deactivate,
    preloadImages:preloadImages,
    doesProductHaveAnyActiveRelatedProducts:doesProductHaveAnyActiveRelatedProducts
  };

  function getData(){
    return ApiService.getData().then(function(res){
      data = res.categories[2];
    });
  }

  //Returns a flattened array of all products (models and related products)
  function getActiveProducts(){
    //make output array
    var output = [];
    //Get active models
    var models = getActiveModels();
    //add each model to the output array as well as all active related products for each model
    models.each(function(model){
      model.type = "model";
      output.push(model);
      if (model.related_products){
        var relatedActive = model.related_products.findAll({
          "active":true
        });
        relatedActive.each(function(relatedProduct){
          relatedProduct.type = 'related';
          output.push(relatedProduct);
        });
      }
    });
    var extras = getActiveExtras();
    extras.each(function(model){
      model.type = "extra";
      output.push(model);
    });
    return output;
  }

  //Returns all models that are either active or have an active related product
  function getActiveModels() {
    //grabs all models, filters by active OR
    return Helpers.getObjectByPath(data,
        'series.models',
        "*",
        {
          clone: false,
          flatten: false,
          // easiest way to get to higher items, series.name in this case would be model.parent.name
          keepParents: true,
          // make sure either the product itself is active
          // or that it has at least one active related product to be included
          filter: function filterActive(product) {
            return product.active;
          }
        }
    );
  }

  function getDisplayName(){
    return data.type + ' ' + data.name + ' Handpieces';
  }

  function getDescription(){
    return data.description;
  }

  function getSeries(){
    return data.series;
  }

  function getTooltips(){
        return data.tooltips;
  }

  function stepIntegrity(step){
    if (step <= 2 && data==undefined){
      return false;
    }
    if (step >= 3){
      if (getActiveModels().length == 0){
        return false;
      }
    }
    return true;
  }

  //Determines if a thing (model or related product) has any active related products
  function doesProductHaveAnyActiveRelatedProducts(product){
    if (product.related_products){
      if (product.related_products.find({'active':true})){
        return true;
      }
    }
    return false;
  }

  function deactivate(item,type){
    if (type == "model"){
      deactivateModel(item)
    }
    if (type == "related"){
      deactivateRelatedProduct(item)
    }
    if (type == "extra"){
      deactivateExtra(item)
    }
  }

  function deactivateModel(m){
    m.active = false;
    deactivateAllRelatedProductsForModel(m);
    return true;
  }

  function deactivateAllRelatedProductsForModel(model){
    if (model.related_products){
      model.related_products.each(function(product){
        product.active = false;
      })
    }
  }

  function deactivateRelatedProduct(product){
    product.active = false;
    return true;
  }

  function preloadImages(){
    getData().then(function(){
      var loader = ImagePreloadFactory.createInstance();
      var products = Helpers.getObjectByPath(data,'series.models','thumbnail_image_url',{
        clone:false,
        flatten:false,
        keepParents:false
      });
      $log.debug("HighSpeedElectricService: Preparing to preload products",products);
      loader.addImages(products.map('thumbnail_image_url'));
      loader.addImages(products.map('light_image_url'));
      loader.addImages(products.map('spray_ports_image_url'));
      loader.start(function(){
        $log.debug("HighSpeedElectricService: Preloading complete");
      });
    });
  }

  //EXTRAS
  function getExtras(){
    return data.extras;
  }

  function getActiveExtras(){
    return data.extras.findAll({active:true});
  }

  function deactivateExtra(product){
    product.active = false;
    return true;
  }

}
