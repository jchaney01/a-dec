'use strict';
/** @ngInject */
function MaintenanceService($log,$timeout,$http,$q,$state,$rootScope,ApiService,Helpers) {
  $log.debug('MaintenanceService: Init');
  var data;
  return  {
    getData:getData,
    getTitle:getDisplayName,
    getIntro:getDescription,
    getModels:getModels,
    getTooltips:getTooltips,
    stepIntegrity:stepIntegrity,
    getActiveModels:getActiveModels,
    deactivate:deactivate,
    getActiveProducts:getActiveProducts,
    doesProductHaveAnyActiveRelatedProducts:doesProductHaveAnyActiveRelatedProducts
  };

  function getData(){
    return ApiService.getData().then(function(res){
      //Mutate the data schema as needed here.
      data = res.categories[5];
    });
  }

  //Determines if a thing (model or related product) has any active related products
  function doesProductHaveAnyActiveRelatedProducts(product){
    if (product.related_products){
      if (product.related_products.find({'active':true})){
        return true;
      }
    }
    return false;
  }

  //Returns a flattened array of all products (models and related products)
  function getActiveProducts(){
    //make output array
    var output = [];
    //Get active models
    var models = getActiveModels();
    //add each model to the output array as well as all active related products for each model
    models.each(function(model){
      model.type = "model";
      output.push(model);
      if (model.related_products){
        var relatedActive = model.related_products.findAll({
          "active":true
        });
        relatedActive.each(function(relatedProduct){
          relatedProduct.type = 'related';
          output.push(relatedProduct);
        });
      }
    });
    return output;
  }

  //Returns all models that are active
  function getActiveModels() {
    return Helpers.getObjectByPath(data,
        'models',
        "*",
        {
          clone: false,
          flatten: false,
          // easiest way to get to higher items, series.name in this case would be model.parent.name
          keepParents: true,
          // make sure either the product itself is active
          // or that it has at least one active related product to be included
          filter: function filterActive(product) {
            return product.active;
          }
        }
    );
  }

  function getDisplayName(){
    return data.name + ' Products';
  }

  function getModels(){
    return data.models;
  }

  function getTooltips(){
        return data.tooltips;
  }

  function getDescription(){
    return data.description;
  }

  function stepIntegrity(step){
    if (step <= 2 && data==undefined){
      return false;
    }
    if (step >= 3){
      if (getActiveModels().length == 0){
        return false;
      }
    }
    return true;
  }

  function deactivate(item,type){
    if (type == "model"){
      deactivateModel(item)
    }
    if (type == "related"){
      deactivateProduct(item)
    }
  }

  function deactivateModel(m){
    m.active = false;
    deactivateAllRelatedProductsForModel(m);
    return true;
  }

  function deactivateAllRelatedProductsForModel(model){
    if (model.related_products){
      model.related_products.each(function(product){
        product.active = false;
      })
    }
  }

  function deactivateProduct(product){
    product.active = false;
    return true;
  }

}
