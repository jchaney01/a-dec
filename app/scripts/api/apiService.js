'use strict';
/** @ngInject */
function ApiService($log,$timeout,$http,$location) {
  var promise;
  var baseUrl = function(){
          if (
              $location.host() == '0.0.0.0'
          ){
              return '//adechandpieceselector.developmentcmd.com/appdata/service/';
          } else {
              return '//'+$location.host()+'/appdata/service/';
          }
      };
  $log.debug('ApiService: Init', 'base url', baseUrl());
  return {
    getData:function(){
      if (!promise){
          promise = $http.get(baseUrl()+'list.ashx',{
            cache:true,
            transformRequest:function(data){
              return JSON.stringify(data);
            }
          }).then(function(data){
            $log.debug("API Service: Received data from server ",data);
            return data.data;
          });
      }
      return promise;
    },
    getChosenCategory:function(){
      return this.chosenCategory;
    },
    setChosenCategory:function(category){
      return this.chosenCategory = category;
    },
    sendData:function(data){
      $log.debug("API Service: Sending data to "+baseUrl()+'post.ashx becasue detected hostname is '+location.hostname,data);
      return $http({
        method: 'POST',
        url: baseUrl()+'post.ashx',
        data: data
      });
    },
    getSavedList:function(id){
      $log.debug("API Service: Getting saved list from server for ID "+id);
      return $http({
        method: 'GET',
        url: baseUrl()+'get.ashx?unique_id='+id
      });
    }
  };
}
