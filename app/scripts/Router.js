'use strict';

/** @ngInject */
function Router($logProvider,$stateProvider, $urlRouterProvider,$uiViewScrollProvider,AnalyticsProvider) {
    $logProvider.debugEnabled(true);
    AnalyticsProvider.setAccount('UA-2577424-7');
    AnalyticsProvider.trackPrefix('adec-selector');
    AnalyticsProvider.setPageEvent('$stateChangeSuccess');
    AnalyticsProvider.ignoreFirstPageLoad(true);
    $uiViewScrollProvider.useAnchorScroll();
    $urlRouterProvider.otherwise("/step1");
    $stateProvider
        .state('home', {
            url: "/step1",
            resolve:{
                'homeService':function(HomeService){
                  return HomeService.getData();
                }
            },
            views: {
                'main':{
                  templateUrl:'scripts/home/home.html',
                  controller:'HomeController',
                  controllerAs:'vm'
                },
                'cta@home':{
                  templateUrl:'scripts/callouts/promos.html',
                  controller:'CtaController'
                }
            }
        })
        .state('hsAir', {
            url: "/high-speed-air/step-{step}",
            resolve:{
                'highSpeedAirServiceData':function(HighSpeedAirService,ApiService,$log){
                  ApiService.setChosenCategory('hsAir');
                  return HighSpeedAirService.getData();
                }
            },
            onEnter: function($log,$stateParams,HighSpeedAirService,$state){
                if (!HighSpeedAirService.stepIntegrity($stateParams.step)){
                    $state.transitionTo('home');
                }
            },
            views: {
                'main':{
                  templateUrl:function ($stateParams){
                      return 'scripts/hsAir/step' + $stateParams.step + '.html';
                  },
                  controller:'HsAirController',
                  controllerAs:'vm'
                },
                'cta@hsAir':{
                    templateUrl:'scripts/callouts/promos.html',
                    controller:'CtaController'
                }
            }
        })
        .state('lsAir', {
            url: "/low-speed-air/step-{step}",
            resolve:{
                'lowSpeedAirServiceData':function(LowSpeedAirService,ApiService){
                  ApiService.setChosenCategory('lsAir');
                  return LowSpeedAirService.getData();
                }
            },
            onEnter: function($log,$stateParams,LowSpeedAirService,$state){
                if (!LowSpeedAirService.stepIntegrity($stateParams.step)){
                    $state.transitionTo('home');
                }
            },
            views: {
                'main':{
                  templateUrl:function ($stateParams){
                      return 'scripts/lsAir/step' + $stateParams.step + '.html';
                  },
                  controller:'LsAirController',
                  controllerAs:'vm'
                },
                'cta@lsAir':{
                    templateUrl:'scripts/callouts/promos.html',
                    controller:'CtaController'
                }
            }
        })
        .state('hsElectric', {
            url: "/high-speed-electric/step-{step}",
            resolve:{
                'highSpeedElectricServiceData':function(HighSpeedElectricService,ApiService){
                  ApiService.setChosenCategory('hsElectric');
                  return HighSpeedElectricService.getData();
                }
            },
            onEnter: function($log,$stateParams,HighSpeedElectricService,$state){
                if (!HighSpeedElectricService.stepIntegrity($stateParams.step)){
                    $state.transitionTo('home');
                }
            },
            views: {
                'main':{
                  templateUrl:function ($stateParams){
                      return 'scripts/hsElectric/step' + $stateParams.step + '.html';
                  },
                  controller:'HsElectricController',
                  controllerAs:'vm'
                },
                'cta@hsElectric':{
                    templateUrl:'scripts/callouts/promos.html',
                    controller:'CtaController'
                }
            }
        })
        .state('lsElectric', {
            url: "/low-speed-electric/step-{step}",
            resolve:{
                'lowSpeedElectricServiceData':function(LowSpeedElectricService,ApiService){
                  ApiService.setChosenCategory('lsElectric');
                  return LowSpeedElectricService.getData();
                }
            },
            onEnter: function($log,$stateParams,LowSpeedElectricService,$state){
                if (!LowSpeedElectricService.stepIntegrity($stateParams.step)){
                    $state.transitionTo('home');
                }
            },
            views: {
                'main':{
                  templateUrl:function ($stateParams){
                      return 'scripts/lsElectric/step' + $stateParams.step + '.html';
                  },
                  controller:'LsElectricController',
                  controllerAs:'vm'
                },
                'cta@lsElectric':{
                    templateUrl:'scripts/callouts/promos.html',
                    controller:'CtaController'
                }
            }
        })
        .state('hygiene', {
            url: "/hygiene/step-{step}",
            resolve:{
                'hygieneElectricServiceData':function(HygieneService,ApiService){
                  ApiService.setChosenCategory('hygiene');
                  return HygieneService.getData();
                }
            },
            onEnter: function($log,$stateParams,HygieneService,$state){
                if (!HygieneService.stepIntegrity($stateParams.step)){
                    $state.transitionTo('home');
                }
            },
            views: {
                'main':{
                  templateUrl:function ($stateParams){
                      return 'scripts/hygiene/step' + $stateParams.step + '.html';
                  },
                  controller:'HygieneController',
                  controllerAs:'vm'
                },
                'cta@hygiene':{
                    templateUrl:'scripts/callouts/promos.html',
                    controller:'CtaController'
                }
            }
        })
        .state('maintenance', {
            url: "/maintenance/step-{step}",
            resolve:{
                'maintenanceServiceData':function(MaintenanceService){
                  return MaintenanceService.getData();
                }
            },
            onEnter: function($log,$stateParams,MaintenanceService,$state){
                if (!MaintenanceService.stepIntegrity($stateParams.step)){
                    $state.transitionTo('home');
                }
            },
            views: {
                'main':{
                  templateUrl:function ($stateParams){
                      return 'scripts/maintenance/step' + $stateParams.step + '.html';
                  },
                  controller:'MaintenanceController',
                  controllerAs:'vm'
                },
                'cta@maintenance':{
                    templateUrl:'scripts/callouts/promos.html',
                    controller:'CtaController'
                }
            }
        })
        .state('summary-form', {
            url: "/summary-form/{category}",
            resolve:{
              'serviceResolutions':function($q,$stateParams,HygieneService,MaintenanceService,HighSpeedAirService,LowSpeedAirService,HighSpeedElectricService,LowSpeedElectricService){
                if ($stateParams.category == 'hsAir'){return HighSpeedAirService.getData();}
                if ($stateParams.category == 'lsAir'){return LowSpeedAirService.getData();}
                if ($stateParams.category == 'hsElectric'){return HighSpeedElectricService.getData();}
                if ($stateParams.category == 'lsElectric'){return LowSpeedElectricService.getData();}
                if ($stateParams.category == 'maintenance'){return MaintenanceService.getData();}
                if ($stateParams.category == 'hygiene'){return HygieneService.getData();}
              }
            },
            onEnter: function($log,$stateParams,$state,HighSpeedAirService,HygieneService,LowSpeedAirService,HighSpeedElectricService,LowSpeedElectricService,MaintenanceService){
              if ($stateParams.category == 'hsAir' && HighSpeedAirService.stepIntegrity(5)){return;}
              if ($stateParams.category == 'lsAir' && LowSpeedAirService.stepIntegrity(5)){return;}
              if ($stateParams.category == 'hsElectric' && HighSpeedElectricService.stepIntegrity(5)){return;}
              if ($stateParams.category == 'lsElectric' && LowSpeedElectricService.stepIntegrity(5)){return;}
              if ($stateParams.category == 'maintenance' && MaintenanceService.stepIntegrity(5)){return;}
              if ($stateParams.category == 'hygiene' && HygieneService.stepIntegrity(5)){return;}
              $state.transitionTo('home');
            },
            views: {
                'main':{
                    templateUrl:function ($stateParams){
                        return 'scripts/summary/summaryFormTmpl.html';
                    },
                    controller:'SummaryFormController',
                    controllerAs:'vm'
                },
                'cta@summary-form':{
                    templateUrl:'scripts/callouts/promos.html',
                    controller:'CtaController'
                }
              }
        })
        .state('summary', {
            url: "/summary/{id}?confirm",
            resolve:{
              'products':function(ApiService,$stateParams){
                return ApiService.getSavedList($stateParams.id);
              }
            },
            views: {
                'main': {
                  templateUrl: 'scripts/summary/summaryTmpl.html',
                  controller: 'SummaryController',
                  controllerAs: 'vm'
                },
                'cta@summary': {
                  templateUrl: 'scripts/callouts/promos.html',
                  controller:'CtaController'
                }
            }
        });
}

