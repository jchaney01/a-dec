'use strict';
/** @ngInject */
function CtaController($log,$scope,$window) {
  $log.debug('CTAController: Init');
  $scope.clickHandpiecePromo = function(){
    $window.open('http://us.a-dec.com/en/~/media/Adec/Document%20Library/Product%20Information/Sales%20Information/Promotions/Clinical-Promo_USA.pdf');
  };
  $scope.clickFindDealer = function(){
    $window.open('http://us.a-dec.com/en/Company/Contact-Us/A-dec-Contacts');
  }
}