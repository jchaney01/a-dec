'use strict';
/** @ngInject */
function HomeService($log,$timeout,$http,$q,$state,$rootScope,ApiService) {
  $log.debug('HomeService: Init');

  var data;

  return  {
    getData:getData,
    getIntro:getIntro,
    getHsAirDesc:getHsAirDesc,
    getLsAirDesc:getLsAirDesc,
    getHsElectricDesc:getHsElectricDesc,
    getLsElectricDesc:getLsElectricDesc,
    getHygineDesc:getHygineDesc,
    getMaintenanceDesc:getMaintenanceDesc,
    getTitles:getTitles,
    getImages:getImages
  };


  function getHsAirDesc(){
    return data.categories[0].bullets;
  }

  function getLsAirDesc(){
    return data.categories[1].bullets;
  }

  function getHsElectricDesc(){
    return data.categories[2].bullets;
  }

  function getLsElectricDesc(){
    return data.categories[3].bullets;
  }

  function getHygineDesc(){
    return data.categories[4].bullets;
  }

  function getMaintenanceDesc(){
    return data.categories[5].bullets;
  }

  function getIntro() {
      return data.intro;
  }

  function getData(){
    return ApiService.getData().then(function(res){
      data = res;
    });
  }

  function getAirTitle(){
    return data.categories[0].type;
  }

  function getElectricTitle(){
    return data.categories[2].type;
  }

  function getMoreTitle(){
    return "More";
  }

  function getHsAirImage(){
    return data.categories[0].image_url;
  }

  function getLsAirImage(){
    return data.categories[1].image_url;
  }

  function getHsElectricImage(){
    return data.categories[2].image_url;
  }

  function getLsElectricImage(){
    return data.categories[3].image_url;
  }

  function getHygineImage(){
    return data.categories[4].image_url;
  }

  function getMaintenanceImage(){
    return data.categories[5].image_url;
  }

  function getTitles(){
    return {
      "air":getAirTitle(),
      "electric":getElectricTitle(),
      "more":getMoreTitle()
    }
  }

  function getImages(){
    return {
      "hsAir":getHsAirImage(),
      "lsAir":getLsAirImage(),
      "hsElectric":getHsElectricImage(),
      "lsElectric":getLsElectricImage(),
      "hygine":getHygineImage(),
      "maintenance":getMaintenanceImage()
    }
  }

}
