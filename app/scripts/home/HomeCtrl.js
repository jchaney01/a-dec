'use strict';

//HomeController.$inject = ['$log','HomeService','$timeout','$scope','$state'];

/** @ngInject */
function HomeController($log,HomeService,$timeout,$scope,$state,ApiService,HighSpeedAirService,LowSpeedAirService,HighSpeedElectricService,LowSpeedElectricService) {
    $log.debug('HomeController: Init');

    var vm = this;
    vm.chooseCategory = chooseCategory;
    vm.clickNextStep = clickNextStep;
    activate();

    function activate(){
      vm.intro = HomeService.getIntro();
      vm.titles = HomeService.getTitles();
      vm.images = HomeService.getImages();
      vm.hsAirDesc = HomeService.getHsAirDesc();
      vm.lsAirDesc = HomeService.getLsAirDesc();
      vm.hsElectricDesc = HomeService.getHsElectricDesc();
      vm.lsElectricDesc = HomeService.getLsElectricDesc();
      vm.hygineDesc = HomeService.getHygineDesc();
      vm.maintenanceDesc = HomeService.getMaintenanceDesc();
      vm.chosenCategory = ApiService.getChosenCategory();
    }

    function chooseCategory(category){
      $log.debug("HomeController: chooseCategory called for "+category);
      if (vm.chosenCategory == category){
        return vm.chosenCategory = ApiService.setChosenCategory(null);
      } else {
        vm.chosenCategory = ApiService.setChosenCategory(category);
      }
      $log.debug("HomeController: Starting preloading for "+vm.chosenCategory);
      if (vm.chosenCategory == 'hsAir'){
        HighSpeedAirService.preloadImages();
      }
      if (vm.chosenCategory == 'lsAir'){
        LowSpeedAirService.preloadImages();
      }
      if (vm.chosenCategory == 'hsElectric'){
        HighSpeedElectricService.preloadImages();
      }
      if (vm.chosenCategory == 'lsElectric'){
        LowSpeedElectricService.preloadImages();
      }

      $state.go(vm.chosenCategory,{step:2});

    }

    function clickNextStep(){
        $log.debug('HomeController: clickNextStep called');
        if (vm.chosenCategory){
            $state.go(vm.chosenCategory,{
                'step':2
            });
        }
    }

}

