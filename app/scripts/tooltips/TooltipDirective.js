'use strict';

/** @ngInject */
function TooltipDirective ( $log, $modal,$timeout,$rootScope ) {
    return {
        restrict: 'AC',
        scope:{
          body:"@",
          title:"@"
        },
        compile: function ( tElement, tAttrs, transclude ) {
            return function ( $scope, $element, $attributes ) {
              var tooltip = jcTT.create($element, $scope.body || 'Body',{
                skin:'light',
                close:true,
                hideOn: false,
                title:$scope.title,
                radius: false,
                maxWidth: 400,
                showOn: 'click',
                hideOthers: true,
                showDelay: 0,
                hideOnClickOutside: true,
                size: 'large'
              });
              $rootScope.$on('$stateChangeStart',function(){
                tooltip.remove($element);
              });
            };
        }
    }
}
