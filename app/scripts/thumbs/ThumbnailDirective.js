'use strict';

/** @ngInject */
function ThumbnailDirective () {
    return {
        restrict: 'AC',
        templateUrl: 'scripts/thumbs/thumbnailTmpl.html',
        replace: true,
        scope: {
            popular: "@thumbnailPopular",
            image: "@thumbnailImage",
            series: "@thumbnailSeries",
            override_name_noimage: "@thumbnailOverrideNoImageName",
            head_size: "@thumbnailHeadSize",
            rating: "@thumbnailRating",
            msrp: "@thumbnailMsrp",
            model: "@thumbnailModel",
            type:"@thumbnailType"
        },
        compile: function ( tElement, tAttrs, transclude ) {
            return function ( $scope, $element, $attributes ) {
            }
        }
    }
}
